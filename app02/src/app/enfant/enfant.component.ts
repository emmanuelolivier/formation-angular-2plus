import { Component, OnInit, Input, EventEmitter, Output } from '@angular/core';

@Component({
  selector: 'app-enfant',
  templateUrl: './enfant.component.html',
  styleUrls: ['./enfant.component.css']
})
export class EnfantComponent implements OnInit {
  @Input() poneys: Array<string>;
  @Output() waitGagnantCourse: EventEmitter<string> = new EventEmitter();

  constructor() {}

  ngOnInit() {}

  sendGagnant(gagnant: string): void {
    this.waitGagnantCourse.emit(gagnant);
  }
}
