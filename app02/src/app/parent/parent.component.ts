import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-parent',
  templateUrl: './parent.component.html',
  styleUrls: ['./parent.component.css']
})
export class ParentComponent implements OnInit {
  poneys: Array<string>;
  gagnant: string;

  constructor() {}

  ngOnInit() {
    this.poneys = [
      'Twilight Sparkle',
      'Spike',
      'Applejack',
      'Fluttershy',
      'Rarity',
      'Pinkie Pie',
      'Rainbow Dash'
    ];
  }

  waitGagnantCourse(gagnant: string): void {
    this.gagnant = gagnant;
  }
}
