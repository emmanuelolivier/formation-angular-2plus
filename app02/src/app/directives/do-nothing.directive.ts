import { Directive, Input } from '@angular/core';

@Directive({
  selector: '[appDoNothing]'
})
export class DoNothingDirective {
  @Input('logToto')
  set text(value: string) {
    console.log(value);
  }

  @Input('logTata')
  set machin(value: string) {
    console.log(`Machin : ${value}`);
  }
}
