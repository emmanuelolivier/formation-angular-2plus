import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Composant02Component } from './composant02.component';

describe('Composant02Component', () => {
  let component: Composant02Component;
  let fixture: ComponentFixture<Composant02Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Composant02Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Composant02Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
