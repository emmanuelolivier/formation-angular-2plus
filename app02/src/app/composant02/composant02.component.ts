import { Component, OnInit } from "@angular/core";

@Component({
  selector: "app-composant02",
  templateUrl: "./composant02.component.html",
  styleUrls: ["./composant02.component.css"]
})
export class Composant02Component implements OnInit {
  nombres: Array<number>;
  textePourLog: string;

  constructor() {}

  ngOnInit() {
    this.nombres = [2, 5, 7, 9, 12];
    this.textePourLog = "Ça c'est du log !";
  }
}
