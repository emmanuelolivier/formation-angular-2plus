import { Component, OnInit } from "@angular/core";
import { IsInput } from "./../interfaces/is-input";
import { Ville } from "./../interfaces/ville";
import { Title, Meta } from "@angular/platform-browser";
import { MesVillesService } from "../services/mes-villes.service";

@Component({
  selector: "app-composant01",
  templateUrl: "./composant01.component.html",
  styleUrls: ["./composant01.component.css"]
})
export class Composant01Component implements OnInit {
  arrives: Array<string>;
  couleur: string;
  listeVilles: Array<Ville>;
  asyncGreeting: Promise<string>;

  constructor(
    private title: Title,
    private meta: Meta,
    private villes: MesVillesService
  ) {
    this.title.setTitle("Mon appli de course");
    this.meta.addTag({ name: "author", content: "eolivier" });
    this.listeVilles = this.villes.list();
  }

  ngOnInit() {
    this.nouvelleCourse();
    this.asyncGreeting = new Promise(resolve => {
      window.setTimeout(() => resolve("hello"), 1000);
    });
  }

  ajouteNouvelArrivant(
    nomNouvelArrivant: IsInput,
    nouvelleCouleur: string
  ): void {
    if (nomNouvelArrivant.value.length > 0) {
      this.arrives.push(nomNouvelArrivant.value);
      nomNouvelArrivant.value = "";
      nomNouvelArrivant.focus();
      if (this.arrives.length === 1) {
        this.title.setTitle(
          `${this.title.getTitle()} - ${nomNouvelArrivant.value}`
        );
      }
    }
    console.log(this.arrives);
    if (this.arrives.length > 2) {
      this.appliquerCouleur(nouvelleCouleur);
    }
  }
  nouvelleCourse(): void {
    this.arrives = [];
  }
  appliquerCouleur(couleur: string): void {
    this.couleur = couleur;
  }
}
