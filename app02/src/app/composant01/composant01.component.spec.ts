import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Composant01Component } from './composant01.component';

describe('Composant01Component', () => {
  let component: Composant01Component;
  let fixture: ComponentFixture<Composant01Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Composant01Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Composant01Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
