import { TestBed } from '@angular/core/testing';

import { MesVillesService } from './mes-villes.service';

describe('MesVillesService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: MesVillesService = TestBed.get(MesVillesService);
    expect(service).toBeTruthy();
  });
});
