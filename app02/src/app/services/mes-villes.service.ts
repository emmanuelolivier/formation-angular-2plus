import { Injectable } from "@angular/core";
import { Ville } from "./../interfaces/ville";

@Injectable({
  providedIn: "root"
})
export class MesVillesService {
  villes: Array<Ville>;

  constructor() {}

  list() {
    this.villes = [{ name: "Londre" }, { name: "Paris" }, { name: "Nantes" }];
    return this.villes;
  }
}
