import { Pipe, PipeTransform } from "@angular/core";

@Pipe({
  name: "multipliePar"
})
export class MultiplieParPipe implements PipeTransform {
  transform(value: any, multiplicateur?: any): any {
    return value * multiplicateur;
  }
}
