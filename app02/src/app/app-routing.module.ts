import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { Composant01Component } from './composant01/composant01.component';
import { Composant02Component } from './composant02/composant02.component';
import { Page404Component } from './page404/page404.component';
import { AccueilComponent } from './accueil/accueil.component';

const routes: Routes = [
  {
    path: '',
    component: AccueilComponent,
    children: [
      { path: '', pathMatch: 'full', redirectTo: 'c01' },
      { path: 'c01', component: Composant01Component },
      { path: 'c02', component: Composant02Component }
    ]
  },
  { path: '**', redirectTo: 'page404' },
  { path: 'page404', component: Page404Component }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}
