import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { Composant01Component } from './composant01/composant01.component';
import { FromNowPipe } from './pipes/from-now.pipe';
import { Composant02Component } from './composant02/composant02.component';
import { MultiplieParPipe } from './pipes/multiplie-par.pipe';
import { DoNothingDirective } from './directives/do-nothing.directive';
import { ParentComponent } from './parent/parent.component';
import { EnfantComponent } from './enfant/enfant.component';
import { Page404Component } from './page404/page404.component';
import { AccueilComponent } from './accueil/accueil.component';

@NgModule({
  declarations: [
    AppComponent,
    Composant01Component,
    FromNowPipe,
    Composant02Component,
    MultiplieParPipe,
    DoNothingDirective,
    ParentComponent,
    EnfantComponent,
    Page404Component,
    AccueilComponent
  ],
  imports: [BrowserModule, AppRoutingModule],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {}
