import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-parent',
  templateUrl: './parent.component.html',
  styleUrls: ['./parent.component.css']
})
export class ParentComponent implements OnInit {
  nombres: Array<number>;
  nombreMultiplie: number;

  constructor() {}

  ngOnInit() {
    this.nombres = [2, 3, 4, 5, 6];
  }

  multiplie(nombre: number): void {
    this.nombreMultiplie = 2 * nombre;
  }
}
