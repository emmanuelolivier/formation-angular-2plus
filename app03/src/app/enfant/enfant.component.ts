import {
  Component,
  OnInit,
  Input,
  Output,
  EventEmitter,
  OnChanges,
  SimpleChanges
} from '@angular/core';

@Component({
  selector: 'app-enfant',
  templateUrl: './enfant.component.html',
  styleUrls: ['./enfant.component.css']
})
export class EnfantComponent implements OnInit, OnChanges {
  @Input() nombres: Array<number>;
  @Output() nombreAMultiplier: EventEmitter<number> = new EventEmitter();
  tableauAAfficher: any;

  constructor() {}

  ngOnInit() {
    console.log('ngOnInit');
  }

  renvoieNombre(nombre: number) {
    this.nombreAMultiplier.emit(nombre);
  }

  ngOnChanges(changes: SimpleChanges): void {
    //Called before any other lifecycle hook. Use it to inject dependencies, but avoid any serious work here.
    //Add '${implements OnChanges}' to the class.
    this.tableauAAfficher = changes;
    console.log(changes);
  }
}
