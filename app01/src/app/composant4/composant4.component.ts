import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-composant4',
  templateUrl: './composant4.component.html',
  styleUrls: ['./composant4.component.css']
})
export class Composant4Component implements OnInit {
  races: Array<string>;
  poneys: Array<string>;

  constructor() { }

  ngOnInit() {
    this.races = [];
    this.initPoneys();
  }

  addToRaces(): void {
    const index = Math.floor(Math.random() * this.poneys.length);
    const unPoneyAuHasard = this.poneys[index];
    this.poneys.splice(index, 1);
    this.races.push(unPoneyAuHasard);
    console.log(this.races);
  }

  clearRaces(): void {
    this.races.splice(0);
    this.initPoneys();
    console.log(this.races);
  }

  private initPoneys() {
    this.poneys = [
      'Twilight Sparkle',
      'Spike',
      'Applejack',
      'Fluttershy',
      'Rarity',
      'Pinkie Pie',
      'Rainbow Dash']
  }
}
