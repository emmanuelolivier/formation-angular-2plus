# Exercice

Créer une appli qui affiche une liste avec ngFor

# Résumé :

-   `{{}}` : interpolation
-   `[]` : binding
-   `()` : binding evenement
-   `#` : declaration variable
-   `*` : définir directive structurale

# Service :

mettre en commun du code qui sera utilisé par plusieurs composants
notemment les acces base de données

## Title

## Meta

# Pipe

Permet de modifier l'affichage sans modifier la variable d'origine

## déjà inclus :

### json

permet d'afficher en json

### UperCase

### LowerCase

### Titlecase

### Number

formate un nombre au format US (12300 : 12,300)
Il prend des arguments pour completer avec des zéros avant ou apres

### percent

### currency

Permet de formater une somme dans la devis choisie

### Date

Ça transforme une date au format voulu. En entrée ça peut être un objet date ou un nomber de millisecondes.
On peut lui dire le format que l'on veux !

### Async

Retourne une chaîne de caractère vide jusqu'à ce que les données deviennent dispo

# La programmation réactive

C'est programmer pour réagir sur des événements

outes données entrante sera dans un flux.
On peux les écouter, les modifier, etc.
Un flux : séquence ordonnée d'événements
On doit subscribe aux flux

Observable resemble aux Promises, sauf que l'Observable es réutilisable.
Et il faut penser à arreter l'écoute quand on ne s'en sert plus.
La bibliothèque la plus utilisée (et choisie par Angular) est RxJS

## RxJS

On peux faire différentes actions sur un observable :

-   take
-   map
-   filter
-   reduce
-   merge
-   subscribe

Dans les subscribe il y a 3 callback :

-   celui qu'on veux faire ;
-   celui d'erreur ;
-   celui de fin.

## Dans Angular

Il y a un Observable spécifique à Angular : `EventEmiter` qui prend trois arguments (une pour réagir aux évenement, une pour réagir aux erreurs, une pour réagir à la terminaison)

# Les directives

C'est comme un composant, mais sans template (donc sans vue)
Elle aura des selecterus css qui diront où l'activée dans le template
sélecteurs possibles :

-   elements (par exemple footer) ;
-   une classe ;
-   un attribut ;
-   un attribut avec une valeur bien précise ;
-   une combinaison de tout ça... Par exemple :
    -- `footer[color=red]`) : un élément avec tout ça ;
    -- `footer:not(.alert)` : un footer mais sans classe alert ;
    -- `[color],footer.alert` : un elem soit color soit footer.
