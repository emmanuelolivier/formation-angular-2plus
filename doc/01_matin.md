# Formation Angular

## Qu'est-ce qu'Angular ?

maj tous les 6 mois par Google
ajd : v8

ANgular est une Sigle Page Application


## environeement :
VSCode / ST ou Atom

node.js / npm / git / augury

### node.js
plateforme sur moteur JS v8
 permet de dev des app en utilisant du JS

 ### npm 
 gestionaire de paquets officiel de node.js

 ### git
 no comment

 ### augury
 extention du google chrome developer tool la plus utilisée pour déboger des applis Angular

### angular-cli
interface en ligne de commande pour automatiser le flux de travail
```
npm install -g @angular/cli
ng --version
```

### webpack
incontournable qui pemet (entre autre) de fusionner en un seul fichier un code écrits en plusieurs modules.
Transpile ES6 vers ES5, ou TipeScript vers ES5

## Angular CLI
créert une appli
executer le serveur de dev avec supprot LiveReload
Ajouter des composants en ligne de commande
executer des TUnex-components

créer la conf pour le déploiement à la prod

```
ng g component my-new-component
ng g directive my-new-directive
ng g pipe my-new-pipe
ng g service my-new-service
ng g class my-new-class
ng g interface my-new-interface
ng g enum my-new-enum
ng g module my-new-module
```

ng : commande angula
g : générate
ce qu'on veux créer
son nom

## Webpack
https://www.github.com/webpack/docs/wiki/list-of-loaders


## Création nouvelle appli
```
ng new app01
```
avec routing et CSS normal

Ensuite dans le dossier : 
```
ng -serve -o
```
le -o ouvre la page dans le navigateur


# ECMASCRIPT 6

ES6 : coeur du javascript modèrne
Mais il n'est pas encore supporté par tous les navigateurs
On transpile dans la version ES5
avec 
- traceur
- ou babeljs (le plus utilisé)

TypeScript : sur-ensemble typé de JS (par dessus ES6)


En ES5 on avait le hoisting : reomntée des vars tout en haut fonction
dans ES6 on a le mot clé `let` et `const` qui elle ne sont plus hoisted

## création d'objet
un objet retourné comme ça :
```ts
{ name: name, color: color}
```
peut être retourné comme ça plutôt :
```ts
{ name, color}
```

## affectations déstructurées
```ts
const { timeout, isCache } = httpOption;
// equivalent à 
const timeout = httpOption.timeout
const isCache = httpoption.isCache
```
C'est même possible pour des trucs imbriqués et même pour des listes

## paramètres optionnels et valeurs par défaut
Les valeurs par défaut dans la définition des fonctions ont été introduites dans ES6
Mais attention, une valeur `0` ou `""` sont considérés comme des valeurs acceptées.

Les valeurs par défault peuvent êtres des appels à des fonctions !
Voire même des valeurs d'autres paramètres de la fonction (mais déjà définis, cad à gauche)

E
E
E
E
E
### rest operator
les `...`
```ts
function addPoniesInRace(...ponies) {
    for (let pony of ponies) {
        poniesInRace.push(pony)
    }
}
```

le rest operator fonctionne aussi avec les affectations déstructurées
```ts
const [winner, ...losers] = poniesInRace;
// winer sera le premier poney de la liste
// losers sera un tableau avec tous les autres poney
```

### spread operator
inverse du rest, c'est les `...` aussi, mais dans un appel


### Classes
fonctions spéciales `constructor`, `tostring`, ...
Fonction peuvent être statiques
Peut avoir des getter et des setters
y'a le mot clé `super` pour la méthode de la classe mère

### Promises
Objectif : simplifier prog asynchrone.
Promise est `thenable` 
Elle a 3 états :
- pending (en cours) ;
- fulfiled (réalisée) ;
- rejected (rejetée : en erreur quoi).
Pour créer une promesse :
on retourne un `new Promise` d'une fonction avec resolve et success

On peut gérer les erreurs par promise (une fonction d'erreur dans le then) ou globalement avec un `.catch` après tous les `then`

### Arrow functions
ce sont les écriture de fonctions avec `=>`

Un des avantages c'est que le `this` est gardé lorsqu'on utilise une arrow function
Idéal pour fonctions anonymes

### SET et MAP
map : sorte de dict
```ts
const users = new Map();
users.set(cedric.id, cedric)
users.size()
```
set : sorte de liste

on peut itérer sur les map/set

### templating
```ts
const fullname = `Miss ${firstname} ${lastname}`

const template = `
<div>
    <h1>Hello></h1>
</div>`
```

### modules
ranger ses fonctions dans un espace de nommage

On peut `export` et `import` des fonctions

```ts
// Named export
import { start as startRace } from './races_service';
// plus loin
startRace(race);
```

Si y'a _une seule_ chose à exporter (classe, fonction, variable) on défini un exportr default
```ts
// fichier du module
export default class Pony {}

// son utilisation dans un autre fichier
import Pony from './pony';
```

## Un peu plus loin en ES6
JS est dynamiquement typé
Par exemple il est possible de faire
```ts
let pony = "Rainbow Dash"
// Plus loin :
pony = 2
```
sauf que c'est moche.

### Injection de dépendance
Pour pouvoir faire du TU, il faut avoir de l'injection de dépendance

### TypeScript
c'est un sur-ensmble de JS (ES5)
Il essaye de se rendre sur-ensemble d'ES6 depuis la version 2.5 (2015)

fichier sont en `.ts`

#### Type de TS
```ts
let variable: type;
// exemple
const poneyNumber: number = 0;
const poneyName: string = 'Rainbow Dash';
const ponies: Array<Pony> = [new Pony()];
// Quand le type doit pouvori changer :
let changing: any = 2;
changing = true
// Mais plus finement on peux faire :
let changing: number|boolean = 2;
changing = true
```

### Valeurs énumérées (enum)
```ts
enum RaceStatus (Ready, Started, Done)
```
Ensuite on peut utiliser RaceStatus.Started (qui sera égal à 1)

### Return type
```ts
function startRace(race: Race): Race {}
// ça peux aussi ne rien renvoyer
function startRace(race: Race): void {}
```

### Interfaces
```ts
function addScore(player: { score : number }, points: number) : void {
    player.score += points;
}
Interface HasScore {
    score: number;
}
function addScore(player: HasScore, point: number) : void {
    player.score += points;
}
```

### Paramètres optionnels
en TS les paramètres sont vraiment attendus, si on l'envoie pas ça va planter.
Ajouter un `?` à la fin du nom du param le rend optionnel (s'il n'a pas de valeru par défaut, sinon pas la peine)


### autre
On peux dire qu'une classe implémente une ou plusieurs interfaces
Une interface peut étendre une ou plusieurs autres


Utilsier `public` ou `private` dans le cosntructeur permet de créer des attributs de la classe sans avoir à l'écrire. Pratiiiiiique

### décorateurs
UN peu comme en python, ce sont des fonctions appelées sur les fonctions, classes ou autre à coup de `@anotation`


# Components
Les Web Components sont définis par 4 particularités :
- custom_element : la balise créée pour être utilisée ailleurs ;
- shadow DOM : la feuille de style et la logique JS de l'appli ne s'applier pas sur le composant ;
- template :
on peut mettre du template dans une balise `<template></template>`
Ce code ne sera executé qu'une fois appelé par autre chose ;
- HTML import : pouvoir importer des fichiers html depuis un autre

Un componant angular est un web component

# Interpolation
Avec `{{ variableName }}` on récupère dans le template la valeur de la variable

# binding de propriétés
entre crochés `[]`
# evenements
entre parentaises `()`
...
