# Le cycle de vie

## Constructeur

## ngOnChange

Il prend tous les inputs en donnant des infos sur chaque input :

```ts
  ngOnChanges(changes: SimpleChanges): void {
    console.log(changes);
  }
  // Exemple de sortie :
  // { "nombres": { "currentValue": [ 2, 3, 4, 5, 6 ], "firstChange": true } }
```

## ngOnInit

## ngOnDestroy

appelé quand le composant est détruit : utile pour le nettoyage (les unsubscribe apr exemple)

## ngDoCheck

s'execute seulement si la condition mise à l'intérieur est vraie

## ngAfterConcentInit

Une fois que tous les contenus ont été vérifiés

## ngAfterConcentChecked

idem même s'ils n'ont pas changés

## ngAfterViewInit

Une fois que tous les binding ont été vérifié une première fois

## ngAfterViewChecked ?

???

# Envoyer et recevoir des données par HTTP

## Obtenir des données

On utilise `HttpClient` importé depuis `HttpClientModule`.

Il faut l'importer et le mettre dans les `import` de la déclaration du module

Ensuite on peut s'en servir avec les verbes HTTP REST habituel, qui retournent tous des observables.

- get
- post
- put
- patch
- delete
- patch
- head
- jsonp

pour mémoire en RESTFULL le PUT écrase la donnée existante même pour les parties non définies dans les données envoyée, là où le patch ne touchera que aux parties qu'il a dans sa paload, sans vider les partie de l'objet qu'il n'a pas.

On peut utiliser un objet en paramettre dans l'appel qui sera traduit en `?key01=value01&key02=value02`...
Pareil pour les headers.

## Intercepteurs

Cela permet d'intercepter des requêtes ou des réponse et d'y faire une action

Il suffit de la définir et la déclarer dans les providers du module

Ça marche aussi pour les réponses. Pratrique pour rediriger vers la page login si jamais on a reçu une 401 par exemple...

# Routes

On a besoin d'un fichier à part qui contiens toutes les routes `app.routes.ts` ou `app.routes.module.ts`
qui sera importé dans le app.module en mettant un `forRoot(ROUTES)` lors de sa déclaration dans les imports

À l'utilisaton dans le composant :

```ts
this.route.paramMap
  .map((params: ParamMap) => params.get('ponyId'))
  .switchMap(id => this.ponyService.get(id))
  .subscribe(pony => (this.pony = pony));
```

Dans la définition des routes on peut mettre des `children` avec des sous-path. Le path vide étant utilisé pour un appel au parent. Et il est possible de faire uen redirection

## loazy loading

On peut charger des routes dans un module fils avec forChildren dans le routing module
et en déclanrant par exemple :

```ts
{ path: 'admin, loadChildren:'/admin/truc#AdminModule'}
```

# Les formulaires

Un champ du formulaire est représenté par un `FormControl` qui a plusieurs attributs :

dirty : faux jusqu'à ce que l'utilisateur modifie la valeur du champ
permet (combiné au valid) de ne pas mettre les chamsp requis vide en rouge tant que l'usager n'y a aps touché

Si on bosse en `piloté par le template` alors il faut importer le `FormModule` dans notre module
Si on bosse en `piloté par le code` on a besoin du `FormModule` mais aussi du `ReactiveFormModule`

## piloté par le template

ON utilise des mots clés directement dans le template
`#userForm="ngForm"` pour créer l'équivalent du formGroup
Dans la fonction associée au `ngSubmit` on peut balancer `userForm.value`

Dans les champs du formulaire on met `ngModel` pour dire que ce sont des sortes de FormControl de notre FormGroup.

## par le code

faut créer les formControl formGroup et les associés au template

# Validateurs

Lors de la création d'un formControl on peut y ajouter

Par le template on
