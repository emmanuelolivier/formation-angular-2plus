import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class SearchmovieService {
  constructor(private http: HttpClient) {}

  search(title: string, year: number = 0): Observable<any> {
    // let result = {};
    let y = year ? `&y=${year}` : '';

    const key = '&i=tt3896198&apikey=44e4e2dd';
    return this.http.get(
      `http://www.omdbapi.com/?t=${title}${y}&plot=full${key}`
    );
  }
}
