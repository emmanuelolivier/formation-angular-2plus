import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, FormBuilder, ReactiveFormsModule } from '@angular/forms';
import { FilmsComponent } from './films.component';
import { RouterModule } from '@angular/router';
import { SearchformComponent } from './searchform/searchform.component';
import { HttpClientModule } from '@angular/common/http';

@NgModule({
  declarations: [FilmsComponent, SearchformComponent],
  imports: [
    CommonModule,
    RouterModule.forChild([{ path: 'films', component: FilmsComponent }]),
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule
  ],
  providers: [FormBuilder]
})
export class FilmsModule {}
