import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { SearchmovieService } from '../services/searchmovie.service';

@Component({
  selector: 'app-searchform',
  templateUrl: './searchform.component.html',
  styleUrls: ['./searchform.component.css']
})
export class SearchformComponent implements OnInit {
  searchForm: FormGroup;
  filmData: object;

  constructor(
    private fb: FormBuilder,
    private searchService: SearchmovieService
  ) {
    this.searchForm = this.fb.group({
      title: this.fb.control('', Validators.required),
      year: ''
    });
  }

  startSearch(): void {
    this.filmData = this.searchService.search(
      this.searchForm.controls.title.value,
      this.searchForm.controls.year.value
    );
  }

  ngOnInit() {}
}
